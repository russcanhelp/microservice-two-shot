from django.shortcuts import render
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]

# class HatDetailEncoder(ModelEncoder):
#     model = Hat
#     properties = ['fabric', 'style_name', 'color', 'picture_url', 'id']

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ['id', 'fabric', 'style_name', 'color', 'picture_url', 'location']
    encoders = {"location": LocationVODetailEncoder()}

# Create your views here.
@require_http_methods(["GET", "POST"])
def list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder
        )
    else: #POST
        content = json.loads(request.body)
        try:
            print(content)
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            {"hat":hat},
            encoder=HatListEncoder,
            safe=False
        )

@require_http_methods(["GET","DELETE","PUT"])
def show_hat_detail(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            {"hat": hat},
            encoder=HatListEncoder
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Hat.objects.filter(id=id).update(**content)
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )
