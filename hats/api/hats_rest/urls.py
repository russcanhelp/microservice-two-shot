from django.urls import path

from .views import list_hats, show_hat_detail

urlpatterns = [
    path('hats/', list_hats, name="list_hats"),
    path('hats/<int:id>/', show_hat_detail, name="show_hat_detail"),
]