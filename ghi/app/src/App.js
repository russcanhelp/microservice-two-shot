import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatColumns from './HatList';
import HatForm from './HatForm'
import HatDetail from './HatDetail'

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='hats/' element={<HatColumns hats={props.hats} />}></Route>
          <Route path='hats/new/' element= {<HatForm />}></Route>
          <Route path='hats/:id' element={<HatDetail hats={props.hats} />}></Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
