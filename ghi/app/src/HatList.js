import React, { useState, useEffect } from 'react'


function HatList(props) {
    return (
      <div className="col">
        {props.hats.map((data) => {
            return(
          <div key={data.hat.id} className="card mb-3 shadow">
            <a href={`hats/${data.hat.id}/`} ><img src={data.hat.picture_url} className="card-img-top" alt="Hat" /></a>
            <div className="card-body">
              <h5 className="card-title"><a href={`hats/${data.hat.id}/`} >{data.hat.style_name}</a></h5>
              <h6 className="card-subtitle mb-2 text-muted">{data.hat.color}</h6>
              <p className="card-subtitle mb-2 text-muted">{data.hat.fabric}</p>
            </div>
          </div>
            )})}
      </div>
    )}

  const HatColumns = (props) => {
    const [hatColumns, setHatColumns] = useState([[], [], []]);

    const fetchData = async () => {
      const url = 'http://localhost:8090/hats/';

      try {
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();


          const requests = [];
          for (let hat of data.hats) {
            const detailURL = `http://localhost:8090/hats/${hat.id}/`;
            requests.push(fetch(detailURL));
          }

          const responses = await Promise.all(requests);
          const columns = [[], [], []];

          let i = 0;
          for (const hatResponse of responses) {
            if (hatResponse.ok) {
              const details = await hatResponse.json();
              columns[i].push(details);
              i = i + 1;
              if (i > 2) {
                i = 0;
              }
            } else {
              console.error(hatResponse);
            }
          }

          setHatColumns(columns);
        }
      } catch (e) {
        console.error(e);
      }
    };

    useEffect(() => {
      fetchData();
    }, []);

    return (
      <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <img
            className="bg-white rounded shadow d-block mx-auto mb-4"
            src="/logo.svg"
            alt=""
            width="600"
          />
          <h1 className="display-5 fw-bold">Microservice Two Shot!</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">The Bin</p>
          </div>
        </div>
        <div className="container">
          <h2>Hats</h2>
          <div className="row">
            {hatColumns.map((hatList, index) => (
              <HatList key={index} hats={hatList} />
            ))}
          </div>
        </div>
      </>
    );
  };

  export default HatColumns;