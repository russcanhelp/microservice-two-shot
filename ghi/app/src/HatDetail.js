import React, {useState} from 'react'
import { useParams} from 'react-router-dom'

function HatDetail(props) {
    const { id } = useParams()

    const [hats, setHats] = useState(props.hats)

    const hat = hats.find((hat) => hat.id === parseInt(id))

    const fetchData = async () => {
      const url = 'http://localhost:8090/api/hats/'

      const response = await fetch(url)
      if (response.ok) {
        const data = await response.json()
        console.log(data.hats)
        setHats(data.hats)
      }
    };

    const deleteHat = async (event, id) => {
      event.preventDefault();
      const url = `http://localhost:8090/hats/${id}`
      const response = await fetch(url, { method: 'DELETE' })
      if (response.ok) {
        setHats(hats.filter((hat) => hat.id !== id))
      }
    };


  if (!hat) {
    return <p>No hat found with the specified ID.</p>
  }

  return (
    <div>
      <h2>{hat.style_name}</h2>
      <img src={hat.picture_url} alt="Hat" />
      <p>Color: {hat.color}</p>
      <p>Fabric: {hat.fabric}</p>
      <button onClick={(event) => deleteHat(event, hat.id)}>Delete</button>
    </div>
  );
}


export default HatDetail;