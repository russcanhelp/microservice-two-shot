import React, { useState, useEffect } from 'react'

function HatForm(props) {
    const [fabric, setFabric] = useState('')
    const [style_name, setStyleName] = useState('')
    const [color, setColor] = useState('')
    const [picture_url, setPictureUrl] = useState('')
    const [locations, setLocations] = useState([])
    const [location, setLocation] = useState('')


const handleFabric = (event) => {
    const value = event.target.value
    setFabric(value)
}

const handleStyleName = (event) => {
    const value = event.target.value
    setStyleName(value)
}

const handleSetColor = (event) => {
    const value = event.target.value
    setColor(value)
}

const handlePictureUrl = (event) => {
    const value = event.target.value
    setPictureUrl(value)
}

const handleLocationChange = (event) => {
    const value = event.target.value
    setLocation(value)
}

const handleSubmit = async (event) => {
    event.preventDefault()
    const data = {}
    data.fabric = fabric
    data.style_name = style_name
    data.color = color
    data.picture_url = picture_url
    data.location = location

    const hatUrl = 'http://localhost:8090/hats/'
    const fetchConfig = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        },
    }

    const response = await fetch(hatUrl, fetchConfig)
    if (response.ok) {
        const newHat = await response.json()
        setFabric('')
        setStyleName('')
        setColor('')
        setPictureUrl('')
        setLocation('')
        console.log(newHat)
    }
}
const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/'
    const response = await fetch(url)
    if (response.ok) {
        const data = await response.json()
        console.log(data)
        setLocations(data.locations)
    }
}

useEffect(() => {
    fetchData()
}, [])

return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
                <input onChange={handleFabric} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleStyleName} value={style_name} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control"/>
                <label htmlFor="style_name">Style Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleSetColor} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handlePictureUrl} value={picture_url} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture URL</label>
            </div>
            <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                <option>Choose a location</option>
                {locations.map(location => {
                    return (
                        <option key={location.id} value={location.href}>
                            {location.closet_name}
                        </option>
                        );
                    })}
            </select>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
    );

}
export default HatForm
